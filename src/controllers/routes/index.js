const helper  = global.helper;
const express = helper.module.express;
const router  = express.Router();

router.use('/',require(`./welcome`));
router.use('/',require(`./imageUpload`));
router.use('/',require(`./awsImageUpload`));



//Write a loader here to avoid adding manual routes.

//index route
router.get('/', (req, res) => {
  res.json({
    ok: 'Please Enter MountPoint'
  });
});

router.post('/',function(req,res){
  res.json({
    message : "Please Enter MountPoint / EntryPoint"
  })
})

router.get('/health-check', (req, res) => {
  res.json({
    alive: `${req.path}`
  });
});

module.exports = router;