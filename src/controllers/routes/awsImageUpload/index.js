const config = helper.config;

const express = require('express');
const router = express.Router();
const multer = require('multer');
const { safePromise, isValidImageMimeType } = require('../../../utilities');
const services = require('../../../services');
const aws = require('aws-sdk');
const middleware = require('../../../middlewares');
const {imgUpload: rules} = require("../../../../rules");
const isAuthorized = middleware.isAuthorized;
const sanitize = middleware.sanitize;
const validate = middleware.validate;
const dirReadUploadToS3 = services.dirReadUploadToS3;
const imageUploadToS3 = services.imageUploadToS3;

const S3_PATH = config.S3_PATH;

const imageFilter = function (req, file, cb) {
    const validType = isValidImageMimeType.includes(file.mimetype);
    if (!validType) {
        return cb({
            message: "Please upload image Content Type",
            code: 500
        }, false);
    }
    cb(null, true);
}

const upload = multer({
    dest: './uploads',
    fileFilter: imageFilter,
    limits: {
        fileSize: 100 * 1024 * 1024
    }
});

const file = upload.single('file');

router.post('/awsUpload', file, function (req, res) {
    res.json({
        message: "file send to aws"
    })
})

router.post('/dirUploadToS3', isAuthorized, file, sanitize, validate(rules), function (req, res) {
    const body = req.body;
    const file = req.file;
    dirReadUploadToS3(body, file)
        .then(function (result) {
            res.json({
                message: "image uploaded to S3",
                result: {
                    images_names: result
                }
            })
        }).catch(function (err) {
            res.json({
                error: err.message
            })
        })
})

router.post('/imgUploadToS3', isAuthorized, file, sanitize, validate(rules), async function (req, res) {
    const body = req.payload;
    const file = req.file;
    if (!file) {
        return res.json({
            message: "Upload file in Body"
        })
    }
    const [error, result] = await safePromise(imageUploadToS3(body, file))
    if (error) {
        console.log(error);
        return res.json({
            success: false,
            error: error,
        })
    }
    if (body.width || body.height) {
        return res.json({
            message: "file uploaded",
            response: {
                originalImage: `${S3_PATH}/${result.originalImage}`,
                thumbnailImage: {
                    [`${result.thumbKey}`]: `${S3_PATH}/${result.thumbImage}`
                }
            }
        })
    } else {
        return res.json({
            message: "file uploaded",
            response: {
                originalImage: `${S3_PATH}/${result.originalImage}`,
            }
        })
    }
})
module.exports = router;