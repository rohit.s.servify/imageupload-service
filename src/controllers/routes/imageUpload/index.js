const helper = global.helper;
const config = helper.config;
const express = helper.module.express;
const router = express.Router();
const multer = require('multer');
const { imageUpload } = require('../../../services');
const path = require('path');
const {isAuthorized, sanitize, validate } = require('../../../middlewares');
const uploadPath = path.resolve(__dirname, '../../../../uploads');
const { imgUpload:rules } = require("../../../../rules");
const { safePromise, getUrl, isValidImageMimeType } = require('../../../utilities');

const imageFilter = function (req, file, cb) {
    const validType = isValidImageMimeType.includes(file.mimetype);
    if (!validType) {
        return cb({
            message: "Please upload image Content Type",
            code: 500
        }, false);
    }
    cb(null, true);
}

const storage = multer.diskStorage({
    destination: uploadPath,
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
const upload = multer({
    storage: storage,
    fileFilter: imageFilter,
    limits: {
        fileSize: 100 * 1024 * 1024
    }
});

const file = upload.single('file');

router.post("/imgUpload", isAuthorized, file, sanitize, validate(rules), async function (req, res) {
    if (!req.file) {
        return res.json({
            message: "Upload file in Body"
        })
    }
    const body = req.payload;
    const file = req.file;
    const [error, result] = await safePromise(imageUpload(body, file))
    if (error) {
        return res.json({
            success: false,
            message: error.message,
            code: error.code
        })
    }
    const response = {
        success: true,
        message: "file uploaded",
        originalImage: `${getUrl()}/${result.originalImage}`,
    };

    if (body.width || body.height) {
        response.thumbnail = {
            [`${result.thumbKey}`]: `${getUrl()}/${result.thumbImage}`
        }
    } 

    return res.json({
        ...response
    })
})

module.exports = router;