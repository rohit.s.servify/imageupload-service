'use strict';

const welcome = require('./welcome');
const imageUpload = require('./imageUpload');
const awsImageUpload = require('./awsImageUpload');


module.exports = {
    welcome,
    imageUpload,
    dirReadUploadToS3: awsImageUpload.dirReadUploadToS3,
    imageUploadToS3 : awsImageUpload.imageUploadToS3
}