const config  = helper.config;
const aws = require('aws-sdk');
const fs = require('fs');
const path = require('path');
const mime = require('mime-types');
const moment = require('moment');
const imageUpload = require('../imageUpload');
const { safePromise } = require('../../utilities');


const S3_BUCKET = config.S3_BUCKET;
const ACL = config.S3_ACL;
const s3 = new aws.S3({
    accessKeyId: config.ACCESS_KEY_ID,
    secretAccessKey: config.SECRET_ACCESS_KEY,
})


const uploadToS3 = (item) => {
    return new Promise((resolve, reject) => {
        let readFilePath  = item.filePath;
        let key = readFilePath.split("/")[2]; 
        fs.readFile(readFilePath, function (err, data) {
            if (err) {
                reject("error in reading file", err);
            }

            const params = {
                Bucket: S3_BUCKET,
                ACL: ACL,
                Key: key,
                Body: data,
                ContentType: mime.contentType(key)
            };
            s3.putObject(params, (err) => {
                if (err) {
                    reject("putobj", err);
                } else {
                    resolve(item); 
                }
            });
        })
    });
};

const dirReadUploadToS3 = (body, file) => {
    const dir = path.resolve(__dirname, "../../../uploads");
    const directoryToUpload = dir;
    const bucketName = 'my-image-uploaded';
    const filePaths = [];
    const getFilePaths = (dirPath) => {
        fs.readdirSync(dirPath).forEach(function (name) {
            const filePath = path.join(dirPath, name);
            const stat = fs.statSync(filePath);
            if (stat.isFile()) {
                filePaths.push(filePath);
            } else if (stat.isDirectory()) {
                getFilePaths(filePath);
            }
        });
    };
    getFilePaths(directoryToUpload);
    const uploadToS3 = (dirPath, filePath) => {
        return new Promise((resolve, reject) => {
            const key = filePath.split(`${dirPath}/`)[1];
            const params = {
                Bucket: bucketName,
                Key: key,
                //Body: fs.readFileSync(filePath),
                Body: fs.createReadStream(filePath),
                ACL: 'public-read',
                ContentType: mime.contentType(key)
            };
            s3.putObject(params, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(filePath);
                }
            });
        });
    };
    const uploadPromises = filePaths.map((path) =>
        uploadToS3(directoryToUpload, path)
    );
    return new Promise(function (resolve, reject) {
        Promise.all(uploadPromises)
            .then((result) => {
                const response = result.map(function (item) {
                    const temp = item.slice(59)
                    return temp;
                })
                resolve(response)
            })
            .catch(function (err) {
                reject(err);
            });
    })
}

const imageUploadToS3 = async (body, file) => {
    let width;
    let height
    const date = moment().format('DD');
    let fileDetails = []; 
    const [error, result] = await safePromise(imageUpload(body, file));
    if(error){
        return Promise.reject(error);
    }
    if (!body.width && !body.height) {
        const filePath = file.destination + "/" + date + "-" + file.originalname; 
        fileDetails.push({filePath, originalImage: result.originalImage}); 
    } else {
        const filePath = file.destination + "/" + date + "-" + file.originalname;
        const thpath = file.destination + "/" + result.thumbImage;
        width = result.dimension.width;
        height = result.dimension.height;
        fileDetails.push({filePath: thpath, width, height, thumbImage: result.thumbImage})
        fileDetails.push({filePath, originalImage: result.originalImage});
    }

    const uploadPromises = fileDetails.map(item => uploadToS3(item));
    const [promiseError, resultAll] = await safePromise(Promise.all(uploadPromises));
    if(promiseError){
        return Promise.reject(promiseError);
    }
    const response = {}
    resultAll.forEach(function (item) {
        if(!item.width){
            response.originalImage = item.originalImage
        }else{
            response.thumbKey = `${item.width}x${item.height}`;
            response.thumbImage = item.thumbImage;
        }
    })
    return Promise.resolve(response)
}

module.exports = {
    dirReadUploadToS3,
    imageUploadToS3
}