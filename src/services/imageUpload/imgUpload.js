const moment = require('moment');
const sharp = require('sharp');
const mv = require('mv');
const sizeof = require('image-size');

const imgUpload = function (body, file) {
    return new Promise(function (resolve, reject) {
        const filePath = file.path;
        const fileName = file.originalname.split('.')[0];
        const fileExtention = file.originalname.split(".")[1];
        const date = moment().format("DD");
        const originalImage = date + "-" + file.originalname;
        const newFileName = file.destination + "/" + originalImage;

        const width = body.width;
        const height = body.height;
        const dimension = {};
        const originalDimension = sizeof(filePath);

        if(width && height){
            dimension.width = +width;
            dimension.height = +height;
        } else if (width) {
            dimension.width = +width;
            dimension.height = parseInt((width/originalDimension.width)* originalDimension.height);
        } else if (height) {
            dimension.height = +height;
            dimension.width = +parseInt((height/originalDimension.height)* originalDimension.width);
        }

        mv(filePath, newFileName, function (err) {
            if (err) {
                return reject(err);
            }
            if (!width && !height) {
                return resolve({ originalImage });
            }
            const thumbImage = date + "-" + fileName + "-" + dimension.width + "x" + dimension.height + '_th' + "." + fileExtention;
            sharp(newFileName)
                .resize({
                    width: dimension.width,
                    height: dimension.height,
                    fit: sharp.fit.contain
                })
                .toFile('uploads/' + thumbImage, function (error, newImage) {
                    if (error) {
                        return reject(error);
                    }
                    resolve({
                        thumbKey: `${dimension.width}x${dimension.height}`,
                        thumbImage: thumbImage,
                        originalImage: originalImage,
                        dimension,
                        fileName : file.originalname
                    })
                })
        })
    })
}
module.exports = imgUpload;