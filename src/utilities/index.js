const isValidImageMimeType = [
    'image/png',
    'image/jpeg','image/jpg',
    'image/gif',
    'image/svg\+xml',
    'image/webp',
    'video/mp4'
];
module.exports = {
    joiValidate: require('./joi-validate'),
    safePromise: require('./safe-promise'),
    logger: require('./logger'),
    getUrl: require('./get-url'),
    isValidImageMimeType
}